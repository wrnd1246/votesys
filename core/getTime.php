<?php
class getTime{
    function getCurrentTime(){
        date_default_timezone_set('Asia/Taipei');
    	$currentTime = date('Y-m-d H:i:s');
    	return $currentTime;
    }
}
?>