<?php

class voteController extends Controller {
    
    //投票項目頁面相關請求
    function item() {
        $vote = $this->model("voteModel");
        // 新增項目
        if (isset($_POST["addStoreBtn"])) {
            $title        = $_POST["fmTitle"];
            $limit        = $_POST["fmLimit"];
            $insertResult = $vote->addItem ($title, $limit); 
            if ($insertResult === false) {
                exit;
            }
        }
        
        // 顯示該項目所有投票
        $result = $vote-> getAllItem();
        $this-> view("vote/item", $result);
    }
    
    //投票相關請求
    function vote() {
        $vote   = $this->model("voteModel");
        $itemId = $_GET["itemId"];
        
        //投票
        if (isset($_GET["sid"])) {
            $vote   = $this->model("voteModel");
            $sid    = $_GET["sid"];
            $itemId = $_GET["itemId"];
            // 確認當前項目有此id、未用完投票額度、未投過此店家，否則導向錯誤訊息頁面
            if (!$vote->sidInThisItem($itemId, $sid) || $vote->alreadyHitLimit($itemId, $vote->getLimitQuota($itemId)) || $vote->isVoted($itemId, $sid)) {
                $this-> view("vote/error", $vote);
                exit;
            }
            $insertResult = $vote->vote ($itemId, $sid); 
            header("Location:/RD2_Project/vote/vote?itemId=$itemId");
        }
        // 顯示該項目所有投票
        $result = $vote-> getAllVote($itemId);
        $this-> view("vote/vote", $result);
    }
    
}

?>