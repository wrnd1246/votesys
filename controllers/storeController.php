<?php

class storeController extends Controller {
    
    // 新增店家
    function addStore(){
        $store  = $this->model("storeModel");
        $name = $_POST['fmTitle'];
        if($store->isDuplicate($name)){
            $this-> view("vote/error", $store);
            exit;
        }
        $itemId = $_GET["itemId"];
        $name   = $_POST["fmTitle"];
        $tag    = $_POST["fmTag"];
        $insertResult = $store->addStore ($itemId, $name, $tag); 
        if ($insertResult === false) {
            exit;
        }
        header("Location:/RD2_Project/vote/vote?itemId=$itemId");
    }
    
    //回傳所有或未在此投票項目中的店家
    function selectStore(){
        $store  = $this->model("storeModel");
        $itemId = $_GET['itemId'];
        
        //若有選擇店家，新增店家到此投票項目
        if(isset($_POST['btnSendStoreSet'])){
            if($_POST['fmStore'] != null){
                $sid = $_POST['fmStore'];
                $insertResult = $store->addSelectedStore($itemId, $sid);
            }
            header("Location:/RD2_Project/vote/vote?itemId=$itemId");
        }
        
        $selectResult = $store->getAllStore($itemId);
        $this-> view("vote/selectStore", $selectResult);
    }
    
    //回傳特定店家
    function findStore(){
        $store  = $this->model("storeModel");
        $sid = $_GET['sid'];
        $store->findStore($sid);
        $this-> view("vote/editStore", $store);
    }
    
    //更新店家資訊
    function updateStore(){
        $store  = $this->model("storeModel");
        $itemId       = $_GET["itemId"];
        $sid          = $_GET["sid"];
        $name         = $_POST["fmTitle"];
        $tag          = $_POST["fmTag"];
        $updateResult = $store->updateStore ($sid, $name, $tag); 
        header("Location:/RD2_Project/vote/vote?itemId=$itemId");
    }
    
}

?>