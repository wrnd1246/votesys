<?php

class homeController extends Controller {
    
    function index() {
        $this->view("home/vote");
    }
    
    function login() {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $this -> post_Login();
            exit;
        }
        $this->view("home/login");
    }
    
    function register() {
        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $user = $this -> model("userModel");
            $username = $_POST["username"];
            $password = $_POST["password"];
            $password_ck = $_POST["password_ck"];
            if ($user -> isPassRegister($username, $password, $password_ck)){
                header("Location:/RD2_Project/home/login");
            }else{
                $this->view("home/register", $user);
            }
        }
        $this->view("home/register");
    }
    
    function post_Login() {
        $user = $this -> model("userModel");
        $username = $_POST["username"];
        $password = $_POST["password"];
        if ($user -> isPassLogin($username, $password)){
            header("Location:/RD2_Project/vote/item");
        }else{
            $this->view("home/login", $user);
        }
    }
    
    function logout() {
        $user = $this -> model("userModel");
        $user->logout();
        header("Location:/RD2_Project/home/login");
    }
    
}

?>