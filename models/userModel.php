<?php

class userModel {
    public  $username;
    private $password;
    private $password_ck;
    public  $msg;
    private $db;

    public function __construct() {
        require_once ("Database.php");
        $this->db = new Database();
    }
    
    //進行帳密檢查
    public function isPassLogin($username, $password){
        $this->username = $username;
        $this->password = $password;
        $this->msg = "";
        $result = false;
    	if (trim($this->username) == "") {
            $this->msg = "請輸入帳號.<br>";
        }
    	if (trim($this->password) == "") {
            $this->msg .= "請輸入密碼.";
        }
        if ($this->msg == ""){
        	$md5_password = md5($this->password);
        	$stmt = $this->db->getConnection()->prepare("SELECT username FROM users WHERE username = :username AND password = :password");
        	$stmt->bindValue(':username', $this->username);
        	$stmt->bindValue(':password', $md5_password  );
        
        	$row = $this->db->selectSingleRow($stmt);
            if (!$row) {
                $this->msg = "帳號或密碼有誤.";
            }else{
                $_SESSION['userName'] = $row['username'];
                $result = true;
            }
        }
        return $result;
    }
    
    //註冊
    public function isPassRegister($username, $password, $password_ck){
        $this->username    = $username;
        $this->password    = $password;
        $this->password_ck = $password_ck;
        $this->msg = "";
        $result = false;
    	if (trim($this->username) == "") {
            $this->msg = "請輸入帳號.<br>";
        }
    	if (trim($this->password) == "") {
            $this->msg .= "請輸入密碼.<br>";
        }
    	if (trim($this->password_ck) == "") {
            $this->msg .= "請輸入確認密碼.<br>";
        }
        if($this->password != $this->password_ck){
            $this->msg .= "密碼與確認密碼不相同.<br>";
        }
        //檢查此帳號是否已註冊
        if($this->isRegistered($this->username)){
            $this->msg .= "此帳號已被註冊.<br>";
        }
        if ($this->msg == ""){
        	$md5_password = md5($this->password);
        	$stmt = $this->db->getConnection()->prepare("INSERT INTO users 
        	                        SET username = :username 
        	                          , password = :password");
        	$stmt->bindValue(':username', $this->username);
        	$stmt->bindValue(':password', $md5_password  );
            $result = $this->db->insert($stmt);
        }
        return $result;
    }
    
    //已註冊過回傳true
    public function isRegistered($username){
        $stmt = $this->db->getConnection()->prepare("SELECT username FROM users WHERE username = :username");
    	$stmt->bindValue(':username', $username);
    
    	$row = $this->db->selectSingleRow($stmt);
        if ($row) {
            $result = true;
        }
        return $result ? true : false;
    }
    
    public function logout() {
        unset($_SESSION["userName"]);
    }
}
    
    
    

?>