<?php

class voteModel{
    public  $username; 
    public  $msg;
    private $db;
    
    public function __construct() {
        require_once ("Database.php");
        $this->db = new Database(); 
        $this->username = $_SESSION["userName"];
    }
    
    //取得該項目所有店家及投票資訊 
    public function getAllVote($itemId){
        
    	//取得所有此項的店家
        $getVote = $this->db->getConnection()->prepare("SELECT COUNT(v.sid) AS count, s.name, s.itemId, s.sid, s.tag
                                                        FROM store AS s LEFT OUTER JOIN vote AS v 
                                                        ON v.sid = s.sid 
                                                        WHERE s.itemId = :itemId GROUP BY v.sid, s.name ORDER BY count DESC");
        $getVote->bindValue(":itemId", $itemId);
        $getVoteResult = $this->db->select($getVote);
    	
        $voteArray = array();
        foreach($getVoteResult as $row){ 
            $voteArray[]=$row;
        }
        
        //取得所有投此項的帳號
        $getWhoVote = $this->db->getConnection()->prepare("SELECT * FROM vote 
                                                           WHERE itemId = :itemId");
        $getWhoVote->bindValue(":itemId", $itemId);
        $getWhoVoteResult = $this->db->select($getWhoVote);
        
        $whoVotedArray = array();
    	foreach($getWhoVoteResult as $row){
    	    $whoVotedArray[]=$row;
    	}
        return array($voteArray, $whoVotedArray);
    }
    
    //取得所有投票項目
    public function getAllItem(){
        
        $getItems = $this->db->getConnection()->prepare("SELECT * FROM item");
    	$query = $this->db->select($getItems);
    
        $voteArray = array();
        foreach($query as $row){ 
            $voteArray[]=$row;
        }
        return $voteArray;
    }
    
    //新增投票項目
    public function addItem ($title, $limit){
        $getTime  = new getTime();   
        $createdTime  = $getTime->getCurrentTime();

        $stmt = $this->db->getConnection()->prepare("INSERT INTO item 
                                                     SET  itemTitle   = :title 
                                                        , whoCreated  = :whoCreated
                                                        , limitQuota  = :limit
                                                        , createdTime = :createdTime");
        $stmt->bindValue(':title'       , $title               );
        $stmt->bindValue(':limit'       , $limit               );
        $stmt->bindValue(':whoCreated'  , $_SESSION['userName']);
        $stmt->bindValue(':createdTime' , $createdTime         );
    	$result = $this->db->insert($stmt);
    	
        return $result ? true : false;
    }
    
    //投票
    public function vote ($itemId, $sid){
        $getTime  = new getTime();   
        $createdTime  = $getTime->getCurrentTime();

        $stmt = $this->db->getConnection()->prepare("INSERT INTO vote 
                                                      SET  sid         = :sid 
                                                         , itemId      = :itemId
                                                         , whoVoted    = :whoVoted
                                                         , createdTime = :createdTime");
        $stmt->bindValue(':sid'         , $sid                  );
        $stmt->bindValue(':itemId'      , $itemId               );
        $stmt->bindValue(':whoVoted'    , $_SESSION['userName'] );
        $stmt->bindValue(':createdTime' , $createdTime          );
    	$result = $this->db->insert($stmt);
    	
        return $result ? true : false;
    }
    
    //已投過票回傳true
    public function isVoted($itemId, $sid){
        $stmt = $this->db->getConnection()->prepare("SELECT * FROM vote 
                                                     WHERE whoVoted = :username 
                                                     AND   itemId   = :itemId
                                                     AND   sid      = :sid");
    	$stmt->bindValue(':username', $_SESSION['userName'] );
    	$stmt->bindValue(':itemId'  , $itemId               );
    	$stmt->bindValue(':sid'     , $sid                  );
    	
    	$row = $this->db->selectSingleRow($stmt);
    
        if ($row) {
            $this->msg = "嘖嘖！想幹嘛呢？您已投過這家店囉！";
            $result = true;
        }
        return $result ? true : false;
    }
    
    //是否已用完投票額度
    public function alreadyHitLimit ($itemId, $limit){
        $stmt = $this->db->getConnection()->prepare("SELECT COUNT(*) AS hitCount FROM vote 
                                                     WHERE whoVoted   = :username 
                                                     AND   itemId     = :itemId");
    	$stmt->bindValue(':username', $_SESSION['userName'] );
    	$stmt->bindValue(':itemId'  , $itemId               );
    	
    	$row = $this->db->selectSingleRow($stmt);
    
        if ($row['hitCount'] >= $limit) {
            $this->msg = "您已用完投票額度";
            $result = true;
        }
        return $result ? true : false;
    }
    
    //回傳此項目的投票額度
    public function getLimitQuota ($itemId){
        $stmt = $this->db->getConnection()->prepare("SELECT limitQuota FROM item 
                                                     WHERE itemId   = :itemId ");
    	$stmt->bindValue(':itemId' , $itemId );
    	$row = $this->db->selectSingleRow($stmt);
    
        return $row['limitQuota'];
    }
    
    //判斷此投票項目是否有此sid，有->回傳true (防跨項目投票)
    public function sidInThisItem($itemId, $sid){
        $stmt = $this->db->getConnection()->prepare("SELECT COUNT(*) FROM store 
                                                     WHERE itemId = :itemId 
                                                     AND   sid    = :sid");
    	$stmt->bindValue(':sid'   , $sid    );
    	$stmt->bindValue(':itemId', $itemId );
    	
    	$row = $this->db->selectSingleRow($stmt);
        
        $result = true;
        if ($row[0] == 0) {
            $this->msg = "當前投票項目無此店家。";
            $result = false;
        }
        return $result ? true : false;
    }
    
}
?>