<?php
class storeModel{
    public  $username; 
    private $db;
    
    public function __construct() {
        require_once ("Database.php");
        $this->db = new Database(); 
        $this->username = $_SESSION["userName"];
    }
    
    //新增店家
    public function addStore ($itemId, $name, $tag){
        $tagSet = "";
        foreach($tag as $tagRow){
            $tagSet.=$tagRow.",";
        }
        $insertStore = $this->db->getConnection()->prepare("INSERT INTO store 
                                                            SET name       = :name 
                                                              , tag        = :tag
                                                              , itemId     = :itemId
                                                              , whoCreated = :whoCreated");
        $insertStore->bindValue(':name'        , $name                );
        $insertStore->bindValue(':tag'         , $tagSet              );
        $insertStore->bindValue(':itemId'      , $itemId              );
        $insertStore->bindValue(':whoCreated'  , $_SESSION['userName']);
    	$result = $this->db->insert($insertStore);
    	
        return $result ? true : false;
    }
    
    //新增被選擇的店家
    public function addSelectedStore($itemId, $sid){
        foreach($sid as $sidRow){
            $selectStore = $this->db->getConnection()->prepare("SELECT * FROM store WHERE sid = :sid");
            $selectStore->bindValue(':sid', $sidRow);
    	    $selectStore->execute();
    	    $selectResult = $selectStore->fetch();
    	    
            $insertStore = $this->db->getConnection()->prepare("INSERT INTO store 
                                                                SET name         = :name 
                                                                  , tag          = :tag
                                                                  , itemId       = :itemId
                                                                  , whoCreated   = :whoCreated");
            $insertStore->bindValue(':name'        , $selectResult['name'] );
            $insertStore->bindValue(':tag'         , $selectResult['tag']  );
            $insertStore->bindValue(':itemId'      , $itemId               );
            $insertStore->bindValue(':whoCreated'  , $_SESSION['userName'] );
    	    $result = $this->db->insert($insertStore);
        }
        
        return $result ? true : false;                                          
    }
    
    //回傳所有未在此投票項目中的店家
    public function getAllStore ($itemId){
        $selectStore = $this->db->getConnection()->prepare("SELECT * FROM store 
                                                            WHERE name NOT IN
                                                            (
                                                                SELECT name FROM store 
                                                                WHERE itemId = :itemId
                                                            ) 
                                                            GROUP BY name");
        $selectStore->bindValue(':itemId', $itemId );
    	
    	$query = $this->db->select($selectStore);
    	
        $storeArray = array();
        foreach($query as $row){ 
            $storeArray[]=$row;
        }
        
        return $storeArray;
    }
    
    //找出指定店家資訊
    public function findStore ($sid){
        $selectStore = $this->db->getConnection()->prepare("SELECT * FROM store WHERE sid = :sid");
        $selectStore->bindValue(':sid', $sid);
    	
    	$result = $this->db->selectSingleRow($selectStore);
    	$this->name    = $result['name'];
    	$this->tagSet  = $result['tag'];
    }
    
    //更新指定店家資訊
    public function updateStore($sid, $name, $tag){
        $tagSet = "";
        foreach($tag as $tagRow){
            $tagSet.=$tagRow.",";
        }
        $updateStore = $this->db->getConnection()->prepare("UPDATE store 
                                           SET   name = :name 
                                               , tag  = :tag
                                           WHERE sid  = :sid");
        $updateStore->bindValue(':name' , $name   );
        $updateStore->bindValue(':tag'  , $tagSet );
        $updateStore->bindValue(':sid'  , $sid    );
    	$result = $this->db->update($updateStore);
    	
        return $result ? true : false;
    }
    
    //店家名稱重複，回傳true
    public function isDuplicate($name){
        $stmt = $this->db->getConnection()->prepare("SELECT * FROM store WHERE name = :name");
    	$stmt->bindValue(':name', $name );
    
    	$row =  $this->db->selectSingleRow($stmt);
        if ($row) {
            $this->msg = "和目前已經存在的店名重複，請修改。";
            $result = true;
        }
        return $result ? true : false;
    }
    
}
?>