<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php
        include_once('views/include/header.php');
    ?>
    
</head>
<body>
    <?php
        include_once('views/include/nav.php');
    ?>
    <div id="page-wrapper" >
        <div id="page-inner">
            <!-- Form Elements -->
            <div class="row">
                <div class="col-md-12">
                    <form role="form" id="form" method="post" enctype="multipart/form-data" action="/RD2_Project/store/updateStore?itemId=<?=$_GET['itemId']?>&sid=<?=$_GET['sid']?>">
                        <div class="form-group">
                            <label style="width: 80px;">店家名稱</label>
                            <label class="text-inline">
                                <input size="50" class="form-control" id="fmTitle" name="fmTitle" value="<?=$data->name?>" />
                            </label>
                        </div>
                        <label class="text-inline">
                            <div class="input_fields_wrap">
                                <label style="width: 80px;">標籤</label>
                                <button class="btn add_field_button btn-primary">新增欄位</button>
                                <?php
                	            $tagSet = array();
                                $tagSet = explode(',', rtrim($data->tagSet, ','));
                                foreach($tagSet as $tagRow){
                                ?>
                                    <div>
                                        <label style="width: 80px;"></label>
                                        <input style="width:200px;display: inline-block;" value="<?=$tagRow?>"  class="form-control" type="text" name="fmTag[]">
                                        <a href="#" class="remove_field">移除</a>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                        </label>
                        <br>
                        <button type="submit" name="saveStoreBtn" id="saveStoreBtn" class="btn saveStoreBtn btn-default">儲存</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</body>
</html>
