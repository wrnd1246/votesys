<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php
        include_once('views/include/header.php');
    ?>
    
</head>
<body>
    <?php
        include_once('views/include/nav.php');
    ?>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
        <div id="page-inner">
            <!-- Form Elements -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <button class="btn btn-info" id="addHWBtn" data-toggle="collapse" data-target="#mybody">新增店家>></button> 
                    <a class="btn btn-info" href="/RD2_Project/store/selectStore?itemId=<?=$_GET['itemId']?>">選擇已存在的店家</a>        
                </div>
                <div id="mybody" class="panel-body collapse">
                    <div class="row">
                        <div class="col-md-12">
                            <form role="form" id="form" method="post" enctype="multipart/form-data" action="/RD2_Project/store/addStore?itemId=<?=$_GET['itemId']?>">
                                <div class="form-group">
                                    <label style="width: 80px;">店家名稱</label>
                                    <label class="text-inline">
                                        <input size="50" class="form-control" id="fmTitle" name="fmTitle" />
                                    </label>
                                </div>
                                <div class="input_fields_wrap">
                                    <label style="width: 80px;">標籤</label>
                                    <label class="text-inline">
                                        <input style="width:200px;display: inline-block;"  class="form-control" type="text" name="fmTag[]">
                                        <button class="btn add_field_button btn-primary">新增欄位</button>
                                    </label>
                                </div>
                                <button type="submit" name="addStoreBtn" id="addStoreBtn" class="btn addStoreBtn btn-default">確定</button>
                                <button type="reset" class="btn btn-primary" data-toggle="collapse" data-target="#mybody">取消</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- End Form Elements -->
        	<div class="row">
            <?php     
                $sn=1;
                foreach($data[0] as $row){
                    $iVoted = false;
            ?>
            	    <div class="col-md-3" style="box-sizing: border-box; border:2px solid; margin:-1px -1px; min-height:200px;">
            	        <div style="float:right;">
            	            <a href="/RD2_Project/store/findStore?itemId=<?=$row['itemId']?>&sid=<?=$row['sid']?>" type="button" data-id=<?=$row['sid']?> class="btn btn-default editButton">編輯</a>
            	        </div>
            	        序號：<?=$sn++?><br>
            	        店家：<?=$row['name']?><br>
            	        票數：<?=$row['count']?><br>
            	        <div style="min-height:90px;">
            	            <?php
            	            $tagSet = array();
                            $tagSet = explode(',', $row['tag']);
                            foreach($tagSet as $tagRow){
                            ?>
            	                <span class="label label-warning"><?=$tagRow?></span>
                            <?php
                            }
                            ?>
            	        </div>
                        <!-- Trigger the modal with a button -->
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal<?=$row['sid']?>">誰投此項</button>
                        
                        <!-- Modal -->
                        <div class="modal fade" id="myModal<?=$row['sid']?>" role="dialog">
                            <div class="modal-dialog"  style="width: 200px;">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">誰投此項</h4>
                                    </div>
                                    <div class="modal-body">
                                        <?php
                                        foreach($data[1] as $whoVoted){
                                            if ($whoVoted['sid'] == $row['sid']) {
                                                if ($whoVoted['whoVoted'] == $_SESSION['userName']) {
                                                    $iVoted = true;
                                                }
                                        ?>
                                                <p><?=$whoVoted['whoVoted']?></p>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
            	        <a href="vote?itemId=<?=$row['itemId']?>&sid=<?=$row['sid'] ?>" rule = "button" class="<?= $iVoted ? 'btn btn-primary disabled' : 'btn btn-primary'?>">
            	            <span class="glyphicon glyphicon-hand-up" aria-hidden="true"></span>&nbsp;
            	            <span class="title1"><b><?= $iVoted ? '已投' : '投票'?></b></span>
            	        </a>
            	    </div>
            <?php
                }
                $sn=0;
            ?>
        	</div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</body>
</html>
