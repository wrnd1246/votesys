<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php
        include_once('views/include/header.php');
    ?>
</head>
<body>
    <?php
        include_once('views/include/nav.php');
    ?>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
        <div id="page-inner">
                <!-- Form Elements -->
                <div class="row">
                    <div class="col-md-12">
                        <form role="form" id="form" method="post" enctype="multipart/form-data" action="">
                            <label>新增投票項目名稱</label>
                            <label class="text-inline">
                                <input size="30" class="form-control" id="fmTitle" name="fmTitle" />
                            </label>
                            <label>每人投票額度</label>
                            <label class="text-inline">
                                <input size="10" class="form-control" id="fmLimit" name="fmLimit" />
                            </label>
                            <button type="submit" name="addStoreBtn" id="addStoreBtn" class="btn addStoreBtn btn-default">確定</button>
                            <button type="reset" class="btn btn-primary" data-toggle="collapse" data-target="#mybody">重設</button>
                        </form>
                    </div>
                </div>
            <!-- End Form Elements -->
        
            <!-- /. ROW  -->
            <div class="row">
                    
                <div class="col-md-12"  >
                    <h3>所有投票項目</h3>
                    <table class="table table-striped title1">
                        <tr>
                            <td><b>序號</b></td>
                            <td><b>投票項目</b></td>
                            <td><b>每人投票額度</b></td>
                            <td><b>建立者</b></td>
                            <td><b>建立時間</b></td>
                            <td></td>
                        </tr>
                    <?php        
                        $sn=1;
                        foreach($data as $row){
                    ?>
                    	<tr>
                    	    <td><?= $sn++ ?></td>
            	            <td><?= $row['itemTitle']?></td>
            	            <td><?= $row['limitQuota']?></td>
            	            <td><?= $row['whoCreated']?></td>
            	            <td><?= $row['createdTime']?></td>
                            <td>
                        	    <b><a href="vote?itemId=<?=$row['itemId']?>" rule = "button" class="btn btn-primary">
                        	          <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>&nbsp;
                        	          <span class="title1"><b>進入投票</b></span></a>
                        	    </b>
                    	    </td>
                    	</tr>
                    <?php
                        }
                        $sn=0;
                    ?>
                    </table>
                    
                </div>
            </div>
            <!-- /. ROW  -->
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
   
</body>
</html>
