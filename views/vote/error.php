<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php
        include_once('views/include/header.php');
    ?>
</head>
<body>
    <!--<div id="wrapper">-->
        <?php
            include_once('views/include/nav.php');
        ?>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
            	<label style="color:red"><?= $data->msg ?></label>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    <!--</div>-->
    <!-- /. WRAPPER  -->
</body>
</html>
