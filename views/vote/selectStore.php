<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php
        include_once('views/include/header.php');
    ?>
</head>
<body>
    <?php
        include_once('views/include/nav.php');
    ?>
    <div id="page-wrapper" >
        <div id="page-inner">
            <!-- Form Elements -->
            <div class="row">
                <div class="col-md-12">
                    <h3>請選擇店家：</h3>
                    <form role="form" id="form" method="post" enctype="multipart/form-data" action="">
                        <div class="form-group">
                            <?php
                            foreach ($data as $row) {
                            ?>
                                <input type="checkbox" id="selectStoreCkBox" name="fmStore[]" style="width: 20px; height: 20px; vertical-align:bottom;" onclick="updateCount()" value="<?=$row['sid']?>"/>
                                <font for="selectStoreCkBox"><?= $row['name'] ?></font><br>
                            <?php
                            }
                            ?>
                        </div>
                        <input name="clickAll" id="clickAll" type="checkbox" style="width: 20px; height: 20px;">全選
                        <button type="submit" name="btnSendStoreSet" id="btnSendStoreSet" class="btn btn-default">選擇店家</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</body>
</html>
