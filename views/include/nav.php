<?php 
session_start();
if (!isset($_SESSION["userName"]))
{
	header("Location: /RD2_Project/home/login");
	exit();
}
?>
<!-- /. NAV TOP  -->
<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" style="float: right" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                    <label style="padding-top:15px; color:white"><?=$_SESSION["userName"]?> 您好！</label>
                    </li>
                    <li>
                        <a href="/RD2_Project/vote/item">投票項目</a>
                    </li>
                    <li>
                        <a href="/RD2_Project/home/logout">登出</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>