
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>註冊</title>
	<?php
        include_once('views/include/header.php');
    ?>
</head>
<body>
	
	<div class="container">
		<div class="row">
        	<div class="span12">
    			<div class="" id="loginModal">
					<div class="modal-header">
						<h3>已有帳號?
			        		<a class="btn btn-primary" href="/RD2_Project/home/login">登入</a>
						</h3>
					</div>
      				<div class="modal-body">
	        			<label style="color:red" ><?= $data->msg ?></label>
				        <div class="well">
							<form class="form-horizontal" action='' method="POST" id="tab">
								<fieldset>
									<div id="legend">
				                    	<legend class="">Register</legend>
				                  	</div>  
				                  	<div class="control-group">
				                    	<!-- Username -->
				                    	<label class="control-label"  for="username">帳號</label>
				                    	<div class="controls">
				                      		<input type="text" id="username" name="username" value="<?= $data->username ?>" placeholder="" class="input-xlarge">
				                    	</div>
				                  	</div>
				
				                  	<div class="control-group">
				                    	<!-- Password-->
				                    	<label class="control-label" for="password">密碼</label>
					                    <div class="controls">
					                      	<input type="password" id="password" name="password" placeholder="" class="input-xlarge">
					                    </div>
					              	</div>
					              	
				                  	<div class="control-group">
				                    	<!-- Password-->
				                    	<label class="control-label" for="password">確認密碼</label>
					                    <div class="controls">
					                      	<input type="password" id="password_ck" name="password_ck" placeholder="" class="input-xlarge">
					                    </div>
					              	</div>
					              	<br>
				
				
				                  	<div class="control-group">
				                    	<!-- Button -->
				                    	<div class="controls">
					                      	<button class="btn btn-success">建立帳號</button>
					                    </div>
				                  	</div>
								</fieldset>
							</form>
				    	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
